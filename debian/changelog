resteasy (3.6.2-3) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * Add salsa-ci.yml

  [ tony mancill ]
  * Add build-dep on libservlet-api-java (Closes: #1057677)
  * Update java.serverlet maven.rule entry to map to debian version

  [ Emmanuel Bourg ]
  * Standards-Version updated to 4.7.0

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 09 Dec 2024 14:12:40 +0100

resteasy (3.6.2-2) unstable; urgency=medium

  * Team upload.

  [ Timo Aaltonen ]
  * Replace build-dep on libtomcat8-java with
    libgeronimo-annotation-1.3-spec-java.

  [ Emmanuel Bourg ]
  * Standards-Version updated to 4.3.0

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 02 Jan 2019 00:55:47 +0100

resteasy (3.6.2-1) unstable; urgency=medium

  * New upstream release. (Closes: #888081)
    - CVE-2017-7561 (Closes: #873392)
    - Refresh the patch
    - Update Maven rules
    - Add libreactive-streams-java to build-depends
    - Add libgeronimo-validation-1.1-spec-java to build-depends
  * poms: Ignore client-jetty, client-microprofile.
  * maven.rules: Updated.
  * control, jaxb-api-compatibility.diff: Fix build, add libjaxb-api-
    java to build-depends.
  * control: Add libhttpasyncclient-java to build-depends.
  * libresteasy-java.poms: Ignore some poms.
  * maven.ignoreRules: Ignore jetty-client.
  * control: Update VCS urls.
  * control: Add libjsonp-java to build-depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 04 Dec 2018 17:12:38 +0200

resteasy (3.1.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Ignore the new security-legacy module
    - Fixes CVE-2016-9606: Yaml unmarshalling vulnerable to remote code
      execution (Closes: #851430)
  * Removed the dependency on glassfish-javaee
  * Standards-Version updated to 4.1.1

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 21 Nov 2017 21:28:12 +0100

resteasy (3.1.0-2) unstable; urgency=medium

  * Enable resteasy-legacy, dogtag-pki needs it. (LP: ##1664457)

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 14 Feb 2017 10:46:33 +0200

resteasy (3.1.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - Updated the Maven rules

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 18 Jan 2017 01:08:55 +0100

resteasy (3.0.19-3) unstable; urgency=medium

  * Team upload.
  * Removed servlet-api-2.5.jar from the Class-Path attribute in the manifests
    (Closes: #850700)
  * Switch to debhelper level 10

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 10 Jan 2017 12:56:01 +0100

resteasy (3.0.19-2) unstable; urgency=medium

  * Team upload.
  * Ignore the dependency on javax.activation (Closes: #839345)

 -- Emmanuel Bourg <ebourg@apache.org>  Sun, 02 Oct 2016 00:42:56 +0200

resteasy (3.0.19-1) unstable; urgency=medium

  * Team upload.
  * New upstream release

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 09 Sep 2016 22:24:31 +0200

resteasy (3.0.18-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Replaced revert-to-jsr250-api.diff with a Maven rule
    - No longer build the removed jaxrs-api module and depend
      on libjaxrs-api-java instead
    - Ignore the jboss-parent pom
    - Ignore the new resteasy-wadl module
    - Ignore the now enabled arquillian module
    - New dependencies on libjboss-logging-java and libjboss-logging-tools-java
    - Refreshed debian/copyright
  * No longer build the unused tjws module (Closes: #830819)
  * Fixed the compilation errors with the Servlet API 3.1
  * Updated debian/watch to track the releases >= 3.0.18

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 20 Jul 2016 13:48:54 +0200

resteasy (3.0.12-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - Updated the Maven rules
  * Fixed the Maven rule for snakeyaml (Closes: #821158)
  * Build with the DH sequencer instead of CDBS
  * Standards-Version updated to 3.9.8 (no changes)
  * Use secure Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 16 Apr 2016 23:14:31 +0200

resteasy (3.0.6-3) unstable; urgency=medium

  * Team upload.
  * Removed the build dependencies on glassfish-activation
    and libservlet2.5-java
  * Build depend on libtomcat8-java instead of libtomcat7-java

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 31 Jul 2015 16:03:37 +0200

resteasy (3.0.6-2) unstable; urgency=high

  * Team upload.
  * Fix CVE-2014-7839: External entities expanded by DocumentProvider
    (Closes: #770544)
  * Standards-Version updated to 3.9.6 (no changes)

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 24 Nov 2014 23:36:45 +0100

resteasy (3.0.6-1) unstable; urgency=medium

  * Team upload.
  * revert-to-jsr250-api.diff: Revert a commit to fix build.
  * libresteasy-java.poms: Ignore json-p-ee7 and resteasy-servlet-
    initializer.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 17 Oct 2014 18:29:12 +0300

resteasy (3.0.1-1) unstable; urgency=medium

  * Initial release (Closes: #734734)

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 04 Sep 2014 22:59:53 +0300
